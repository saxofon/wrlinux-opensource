# Default settings
HOSTNAME 	?= $(shell hostname)
USER		?= $(shell whoami)

# Optional configuration
-include hostconfig-$(HOSTNAME).mk
-include userconfig-$(USER).mk

TOP := $(shell pwd)

SHELL := /bin/bash

# Define V=1 to echo everything
V ?= 0
ifneq ($(V),1)
	Q=@
endif

RM = $(Q)rm -f

WRL ?= https://github.com/WindRiver-OpenSourceLabs
REL ?= master-wr

MACHINE=intel-x86-64

DISTRO=wrlinux

IMAGE=wrlinux-image-glibc-std

WRLS_OPTS += --dl-layers
WRLS_OPTS += --accept-eula yes
WRLS_OPTS += --distros $(DISTRO)
WRLS_OPTS += --machines $(MACHINE)

define bitbake
	cd build ; \
	source ./oe-init-build-env ; \
	bitbake $(1)
endef

define bitbake-task
	cd build ; \
	source ./oe-init-build-env ; \
	bitbake $(1) -c $(2)
endef

all: fs

.PHONY: build
build:
	$(Q)if [ ! -d $@ ]; then \
		mkdir -p $@ ; \
		cd $@ ; \
		git clone --branch $(REL) $(WRL)/wrlinux-x wrlinux-x ; \
		./wrlinux-x/setup.sh $(WRLS_OPTS) ; \
	fi

# create bitbake build
#		sed -i /systemd/d conf/local.conf ; \
#		sed -i /sysvinit/d conf/local.conf ; \
#
.PHONY: build/build
build/build: build $(LAYERS)
	$(Q)if [ ! -d $@ ]; then \
		cd build ; \
		source ./oe-init-build-env ; \
		if [ "$(LAYERS)" != "" ]; then \
		bitbake-layers add-layer $(LAYERS) ; \
		fi ; \
		sed -i s/^MACHINE.*/MACHINE\ =\ \"$(MACHINE)\"/g conf/local.conf ; \
		sed -i s/^\BB_NO_NETWORK.*/BB_NO_NETWORK\ =\ \"0\"/g conf/local.conf ; \
		sed -i s/^\BB_FETCH_PREMIRRORONLY.*/BB_FETCH_PREMIRRORONLY\ =\ \"0\"/g conf/local.conf ; \
	fi

#	echo "ENABLE_KERNEL_DEV=\"1\"" >> conf/bblayers.conf ; \

bbs: build/build
	$(Q)cd build ; \
	source ./oe-init-build-env ; \
	bash || true

fs: build/build
	$(call bitbake, $(IMAGE))

sdk: build/build
	$(call bitbake-task, $(IMAGE), populate_sdk)

esdk: build/build
	$(call bitbake-task, $(IMAGE), populate_sdk_ext)

clean:
	$(RM) -r build/build

distclean:
	$(RM) -r build
